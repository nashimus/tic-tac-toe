# Tic Tac Toe

## About

This is a very naive implementation of Tic Tac Toe, using Rust and the [Piston Game Engine](https://www.piston.rs/).  Currently it supports single player vs CPU. The AI is a random number generator.  The interface tries to maintain a 1:1 aspect ratio and scale based on screen resolution.

## Build and Run

```sh
git clone https://gitlab.com/nashimus/tic-tac-toe
cd tic-tac-toe
cargo run
```

## Screenshots

### Gameplay

![Alt text](img/screenshot01.png "Gameplay")

### Victory

![Alt text](img/screenshot02.png "Gameplay: Victory")

### Loss

![Alt text](img/screenshot03.png "Gameplay: Loss")

### Stalemate

![Alt text](img/screenshot04.png "Gameplay: Stalemate")
