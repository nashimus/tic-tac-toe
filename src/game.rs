extern crate find_folder;
extern crate music;
extern crate piston_window;

use piston_window::*;
use rand;

#[derive(Copy, Clone, Hash, PartialEq, Eq)]
enum Music {
    BackgroundMusic,
}

#[derive(Copy, Clone, Hash, PartialEq, Eq)]
enum Sound {
    WinGame,
    LoseGame,
    Stalemate,
    Click,
}

#[derive(Debug, PartialEq, Copy, Clone)]
enum Turn {
    Player,
    Bot,
    Nobody,
}

#[derive(Copy, Clone)]
struct Cell {
    color: [f32; 4],
    symbol: Turn,
    rect: [f64; 4],
}

pub struct Game {
    win_size: Size,
    cell_height: f64,
    cell_width: f64,
    board: [Cell; 9],
    current_turn: Turn,
    mouse_x: f64,
    mouse_y: f64,
    available_color: [f32; 4],
    player1_cell_color: [f32; 4],
    player2_cell_color: [f32; 4],
    player1_color: [f32; 4],
    player2_color: [f32; 4],
    line_color: [f32; 4],
    game_message1: String,
    game_message2: String,
    game_message3: String,
    game_score_text: String,
    glyphs: Glyphs,
    redraw: bool,
    center: bool,
    dpi_factor: f64,
    monitor_dimensions: Size,
    font_size: u32,
    game_ended: bool,
    pub exit_game: bool,
    winner: Turn,
    player1_victory_count: u32,
    player2_victory_count: u32,
    stalemate_count: u32,
}

impl Game {
    pub fn new(glyphs: Glyphs, dpi_factor: f64, monitor_dimensions: Size) -> Game {
        let mut new_game = Game {
            win_size: monitor_dimensions,
            cell_height: 0.0,
            cell_width: 0.0,
            available_color: [0.0, 0.0, 0.0, 0.0],
            player1_cell_color: [1.0, 1.0, 1.0, 1.0],
            player2_cell_color: [1.0, 1.0, 1.0, 1.0],
            player1_color: [0.0, 0.0, 1.0, 1.0],
            player2_color: [1.0, 0.0, 0.0, 1.0],
            line_color: [0.5, 0.5, 0.5, 1.0],
            board: [Cell {
                color: [0.0, 0.0, 0.0, 0.0],
                symbol: Turn::Nobody,
                rect: [0.0, 0.0, 0.0, 0.0],
            }; 9],
            current_turn: Turn::Player,
            mouse_x: 0.0,
            mouse_y: 0.0,
            game_message1: String::new(),
            game_message2: String::new(),
            game_message3: String::new(),
            game_score_text: String::new(),
            glyphs,
            redraw: true,
            center: true,
            dpi_factor,
            monitor_dimensions,
            font_size: 16,
            game_ended: false,
            exit_game: false,
            winner: Turn::Nobody,
            player1_victory_count: 0,
            player2_victory_count: 0,
            stalemate_count: 0,
        };

        for i in 0..new_game.board.len() {
            new_game.board[i] = Cell {
                color: new_game.available_color,
                symbol: Turn::Nobody,
                rect: [0.0, 0.0, 0.0, 0.0],
            };
        }

        return new_game;
    }

    fn music_init(&self) {
        music::bind_music_file(Music::BackgroundMusic, "./src/assets/Cunning plan.ogg"); //https://opengameart.org/content/cunning-plan-short-seamless-loop
        music::bind_sound_file(Sound::WinGame, "./src/assets/sfx_sounds_powerup3.wav"); //https://opengameart.org/content/512-sound-effects-8-bit-style
        music::bind_sound_file(Sound::LoseGame, "./src/assets/sfx_sounds_negative1.wav"); //https://opengameart.org/content/512-sound-effects-8-bit-style
        music::bind_sound_file(Sound::Stalemate, "./src/assets/sfx_deathscream_human5.wav"); //https://opengameart.org/content/512-sound-effects-8-bit-style
        music::bind_sound_file(Sound::Click, "./src/assets/sfx_coin_single3.wav"); //https://opengameart.org/content/512-sound-effects-8-bit-style
        music::set_volume(0.3);
        music::play_music(&Music::BackgroundMusic, music::Repeat::Forever);
    }

    pub fn run(&mut self, mut window: &mut PistonWindow) {
        music::start::<Music, Sound, _>(16, || {
            self.music_init();

            while let Some(e) = window.next() {
                if self.exit_game {
                    break;
                }
                self.update_state();

                if let Some(_) = e.render_args() {
                    self.render(&mut window, &e);
                }

                if let Some(r) = e.resize_args() {
                    self.resize(r, &mut window);
                }

                if let Some(mouse_rel) = e.mouse_cursor_args() {
                    self.mouse_move(mouse_rel);
                }

                if let Some(button) = e.press_args() {
                    self.player_input(button);
                }
            }
        });
    }

    pub fn render(&mut self, window: &mut PistonWindow, e: &Event) {
        window.set_size([self.win_size.width, self.win_size.height]);
        self.cell_width = window.size().width / 3 as f64;
        self.cell_height = window.size().height / 3 as f64;
        for i in 0..self.board.len() {
            let mut x_pos = (i as f64) % 3.0;
            let mut y_pos = ((i as f64) - x_pos) / 3.0;
            x_pos *= self.cell_width;
            y_pos *= self.cell_height;

            self.board[i].rect = [x_pos, y_pos, self.cell_width, self.cell_height];
        }

        let win_size = window.size();
        self.game_score_text = format!(
            "Player 1: {}                  Player 2: {}                  Draw: {}",
            self.player1_victory_count, self.player2_victory_count, self.stalemate_count
        );
        window.draw_2d(e, |c, g, _device| {
            clear([1.0; 4], g);
            //draw board
            for i in 0..self.board.len() {
                rectangle(self.board[i].color, self.board[i].rect, c.transform, g);
                let shape_x = self.board[i].rect[0] + self.cell_width / 3.8;
                let shape_y = self.board[i].rect[1] + self.cell_height / 3.8;
                let shape_x_max = shape_x + self.cell_width / 2.0;
                let shape_y_max = shape_y + self.cell_height / 2.0;
                let x_thickness = self.cell_width / 8.0;
                let o_thickness = self.cell_width / 16.0;
                let o_scaling_factor = 0.75;

                // draw symbol
                match self.board[i].symbol {
                    Turn::Player => {
                        // first slash of X \
                        polygon(
                            self.player1_color,
                            &[
                                [shape_x, shape_y],
                                [shape_x + x_thickness, shape_y],
                                [shape_x_max, shape_y_max],
                                [shape_x_max - x_thickness, shape_y_max],
                            ],
                            c.transform,
                            g,
                        );
                        // second slash of X /
                        polygon(
                            self.player1_color,
                            &[
                                [shape_x_max, shape_y],
                                [shape_x_max - x_thickness, shape_y],
                                [shape_x, shape_y_max],
                                [shape_x + x_thickness, shape_y_max],
                            ],
                            c.transform,
                            g,
                        );
                    }
                    Turn::Bot => {
                        circle_arc(
                            self.player2_color,
                            o_thickness,
                            0.0,
                            std::f64::consts::PI * 2.0 - 0.00001,
                            [
                                shape_x + (self.cell_width * o_scaling_factor / 12.0),
                                shape_y + (self.cell_height * o_scaling_factor / 12.0),
                                (shape_x_max - shape_x) * o_scaling_factor, //circle width
                                (shape_y_max - shape_y) * o_scaling_factor, //circle height
                            ],
                            c.transform,
                            g,
                        );
                    }
                    Turn::Nobody => {}
                }
            }

            //vertical lines
            let line_thickness = 1.5;
            rectangle(
                self.line_color,
                [
                    self.cell_width - (line_thickness / 2.0),
                    0.0,
                    line_thickness,
                    win_size.height,
                ],
                c.transform,
                g,
            );

            rectangle(
                self.line_color,
                [
                    (self.cell_width * 2.0) - (line_thickness / 2.0),
                    0.0,
                    line_thickness,
                    win_size.height,
                ],
                c.transform,
                g,
            );

            //horizontal lines
            rectangle(
                self.line_color,
                [
                    0.0,
                    self.cell_height - (line_thickness / 2.0),
                    win_size.width,
                    line_thickness,
                ],
                c.transform,
                g,
            );

            rectangle(
                self.line_color,
                [
                    0.0,
                    (self.cell_height * 2.0) - (line_thickness / 2.0),
                    win_size.width,
                    line_thickness,
                ],
                c.transform,
                g,
            );

            // message background
            if self.game_message1.len() > 0 {
                let r = [0.0, 0.0, win_size.width, win_size.height];
                rectangle([1.0, 1.0, 1.0, 0.5], r, c.transform, g);
            }

            //draw message
            let msg1_width = character::CharacterCache::width(
                &mut self.glyphs,
                self.font_size,
                &*self.game_message1,
            )
            .unwrap();
            let msg1_x = (win_size.width / 2.0) - msg1_width / 2.0;
            let msg1_transform = c.transform.trans(msg1_x, win_size.height / 2.5);
            text::Text::new_color([0.0, 0.0, 0.0, 1.5], self.font_size)
                .draw(
                    &self.game_message1,
                    &mut self.glyphs,
                    &c.draw_state,
                    msg1_transform,
                    g,
                )
                .unwrap();

            let msg2_width = character::CharacterCache::width(
                &mut self.glyphs,
                self.font_size,
                &*self.game_message2,
            )
            .unwrap();
            let msg2_x = (win_size.width / 2.0) - msg2_width / 2.0;
            let msg2_transform = c.transform.trans(msg2_x, win_size.height / 1.55);
            text::Text::new_color([0.0, 0.0, 0.0, 1.5], self.font_size)
                .draw(
                    &self.game_message2,
                    &mut self.glyphs,
                    &c.draw_state,
                    msg2_transform,
                    g,
                )
                .unwrap();

            let msg3_width = character::CharacterCache::width(
                &mut self.glyphs,
                self.font_size,
                &*self.game_message3,
            )
            .unwrap();
            let msg3_x = (win_size.width / 2.0) - msg3_width / 2.0;
            let msg3_transform = c.transform.trans(msg3_x, win_size.height / 1.37);
            text::Text::new_color([0.0, 0.0, 0.0, 1.5], self.font_size)
                .draw(
                    &self.game_message3,
                    &mut self.glyphs,
                    &c.draw_state,
                    msg3_transform,
                    g,
                )
                .unwrap();

            let game_score_text_width = character::CharacterCache::width(
                &mut self.glyphs,
                self.font_size / 2,
                &*self.game_score_text,
            )
            .unwrap();
            let game_score_text_x = (win_size.width / 1.97) - game_score_text_width / 2.0;
            let game_score_text_transform = c
                .transform
                .trans(game_score_text_x, self.font_size as f64 / 2.0);
            text::Text::new_color([0.0, 0.0, 0.0, 1.5], self.font_size / 2 - 1)
                .draw(
                    &self.game_score_text,
                    &mut self.glyphs,
                    &c.draw_state,
                    game_score_text_transform,
                    g,
                )
                .unwrap();

            // Update glyphs before rendering.
            let _glyph_flush_result = &self.glyphs.factory.encoder.flush(_device);
        });
    }

    pub fn resize(&mut self, r: ResizeArgs, window: &mut PistonWindow) {
        if self.center {
            let x = ((self.monitor_dimensions.width / self.dpi_factor) - r.window_size[0]) / 2.0;
            let y = ((self.monitor_dimensions.height / self.dpi_factor) - r.window_size[1]) / 2.0
                * 0.95;
            window.set_position([x as i32, y as i32]);
        }
        self.font_size = ((36.0 / 480.0) * window.size().width) as u32;

        if r.window_size[0] != r.window_size[1] {
            // try to maintain aspect ratio 1:1
            let xy = ((r.window_size[0] + r.window_size[1]) / 2.0) as i32 as f64;
            self.win_size.width = xy;
            self.win_size.height = xy;
        }
    }

    fn play_again(&mut self) -> bool {
        self.game_message2 = String::from("Click to play again.");
        self.game_message3 = String::from("Esc to exit.");
        false
    }

    pub fn update_state(&mut self) {
        self.check_for_victory();
        if self.winner != Turn::Nobody && self.game_ended == false {
            self.game_ended = true;
            match self.winner {
                Turn::Player => {
                    self.game_message1 = String::from("You won!");
                    music::play_sound(&Sound::WinGame, music::Repeat::Times(0), 0.2);
                }
                Turn::Bot => {
                    self.game_message1 = String::from("You lost!");
                    music::play_sound(&Sound::LoseGame, music::Repeat::Times(0), 0.2);
                }
                Turn::Nobody => {}
            };
            if self.play_again() {
                self.reset_board();
            }
        }
        if !self.moves_remaining() && self.game_ended == false {
            self.game_ended = true;
            self.game_message1 = String::from("Stalemate");
            music::play_sound(&Sound::Stalemate, music::Repeat::Times(0), 0.5);
            if self.play_again() {
                self.reset_board();
            }
        }
        if self.game_ended == false {
            match self.current_turn {
                Turn::Player => {
                    //self.game_message = String::from("Player1's turn!");
                }
                Turn::Bot => {
                    //self.game_message = String::from("Player2's turn!");
                    let bot_move = self.get_bot_move();
                    self.board[bot_move].color = self.player2_cell_color;
                    self.board[bot_move].symbol = Turn::Bot;
                    self.current_turn = self.get_next_turn();
                }
                Turn::Nobody => {}
            };
        }
    }

    pub fn mouse_move(&mut self, mouse_rel: [f64; 2]) {
        self.mouse_x = mouse_rel[0];
        self.mouse_y = mouse_rel[1];
    }

    pub fn player_input(&mut self, button: Button) {
        self.redraw = true;
        match button {
            Button::Keyboard(key) => {
                //println!("Keyboard key pressed: {:?}", key);
                match key {
                    Key::Escape => {
                        self.exit_game = true;
                    }
                    _ => self.redraw = false,
                }
                self.redraw = false;
            }
            Button::Mouse(_btn) => {
                /*
                println!(
                    "Mouse click {:?} at: {}, {}",
                    _btn, self.mouse_x, self.mouse_y
                );*/
                if self.game_ended {
                    self.reset_board();
                } else {
                    self.mouse_click(self.mouse_x, self.mouse_y);
                }
            }
            Button::Controller(_) => {
                println!("Controller button pressed: {:?}", button); // doesn't work with Logitech F310 on ppc64le
                self.redraw = false;
            }
            _ => {
                println!("Unknown button pressed: {:?}", button); // doesn't work with Logitech F310 on ppc64le
                self.redraw = false;
            }
        }
    }

    fn mouse_click(&mut self, x: f64, y: f64) -> bool {
        //determine clicked cell
        for i in 0..self.board.len() {
            if self.current_turn == Turn::Player
                && x >= self.board[i].rect[0]
                && x < self.board[i].rect[0] + self.board[i].rect[2]
                && y >= self.board[i].rect[1]
                && y < self.board[i].rect[1] + self.board[i].rect[3]
            {
                if self.board[i].symbol == Turn::Nobody {
                    self.board[i].color = self.player1_cell_color;
                    self.board[i].symbol = Turn::Player;
                    self.current_turn = self.get_next_turn();
                    music::play_sound(&Sound::Click, music::Repeat::Times(0), 0.25);
                    return true;
                } else {
                    self.redraw = false;
                };
            }
        }
        return false;
    }

    fn get_bot_move(&self) -> usize {
        let mut bot_move = rand::random::<usize>() % 9;

        while self.board[bot_move].symbol != Turn::Nobody {
            bot_move = rand::random::<usize>() % 9;
        }
        return bot_move;
    }

    fn get_next_turn(&self) -> Turn {
        match self.current_turn {
            Turn::Player => Turn::Bot,
            Turn::Bot => Turn::Player,
            Turn::Nobody => Turn::Nobody,
        }
    }

    fn moves_remaining(&self) -> bool {
        let mut moves = 0;

        for i in 0..self.board.len() {
            if self.board[i].symbol == Turn::Nobody {
                moves += 1;
            }
        }
        return moves > 0;
    }

    fn check_for_victory(&mut self) {
        //same row 1
        if self.board[0].symbol != Turn::Nobody
            && self.board[0].symbol == self.board[1].symbol
            && self.board[1].symbol == self.board[2].symbol
        {
            self.winner = self.board[0].symbol;
        }

        //same row 2
        if self.board[3].symbol != Turn::Nobody
            && self.board[3].symbol == self.board[4].symbol
            && self.board[4].symbol == self.board[5].symbol
        {
            self.winner = self.board[3].symbol;
        }

        //same row 3
        if self.board[6].symbol != Turn::Nobody
            && self.board[6].symbol == self.board[7].symbol
            && self.board[7].symbol == self.board[8].symbol
        {
            self.winner = self.board[6].symbol;
        }

        //same column 1
        if self.board[0].symbol != Turn::Nobody
            && self.board[0].symbol == self.board[3].symbol
            && self.board[3].symbol == self.board[6].symbol
        {
            self.winner = self.board[0].symbol;
        }

        //same column 2
        if self.board[1].symbol != Turn::Nobody
            && self.board[1].symbol == self.board[4].symbol
            && self.board[4].symbol == self.board[7].symbol
        {
            self.winner = self.board[1].symbol;
        }

        //same column 3
        if self.board[2].symbol != Turn::Nobody
            && self.board[2].symbol == self.board[5].symbol
            && self.board[5].symbol == self.board[8].symbol
        {
            self.winner = self.board[2].symbol;
        }

        // same diagonal \
        if self.board[4].symbol != Turn::Nobody
            && self.board[0].symbol == self.board[4].symbol
            && self.board[4].symbol == self.board[8].symbol
        {
            self.winner = self.board[4].symbol;
        }

        // same diagonal /
        if self.board[4].symbol != Turn::Nobody
            && self.board[2].symbol == self.board[4].symbol
            && self.board[4].symbol == self.board[6].symbol
        {
            self.winner = self.board[4].symbol;
        }
    }

    fn reset_board(&mut self) {
        if self.game_ended == true {
            match self.winner {
                Turn::Player => self.player1_victory_count += 1,
                Turn::Bot => self.player2_victory_count += 1,
                Turn::Nobody => self.stalemate_count += 1,
            }
        }
        self.game_ended = false;
        let player_turn = rand::random::<usize>() % 2;
        if player_turn == 0 {
            self.current_turn = Turn::Player
        } else {
            self.current_turn = Turn::Bot
        }

        for i in 0..self.board.len() {
            self.board[i].color = self.available_color;
            self.board[i].symbol = Turn::Nobody;
        }
        self.game_message1 = String::new();
        self.game_message2 = String::new();
        self.game_message3 = String::new();
        self.winner = Turn::Nobody;
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    //TODO(): Refactor this terrible way of doing this.
    fn game_setup() -> Game {
        let mut window: PistonWindow = WindowSettings::new("Tic Tac Toe", [90.0, 90.0])
            .exit_on_esc(true)
            .build()
            .unwrap();

        let monitor_dimensions = window
            .window
            .ctx
            .window()
            .get_current_monitor()
            .get_dimensions();

        let mon_dim = Size {
            width: monitor_dimensions.width,
            height: monitor_dimensions.height,
        };

        let dpi_factor = window
            .window
            .ctx
            .window()
            .get_current_monitor()
            .get_hidpi_factor();

        window.set_max_fps(60);

        let new_size = Size {
            width: 0.5 * monitor_dimensions.width / dpi_factor,
            height: 0.5 * monitor_dimensions.width / dpi_factor,
        };
        window.set_size(new_size);

        let x = ((monitor_dimensions.width / dpi_factor) - window.size().width) / 2.0;
        let y = ((monitor_dimensions.height / dpi_factor) - window.size().height) / 2.0 * 0.95;
        window.set_position([x as i32, y as i32]);

        let assets = find_folder::Search::ParentsThenKids(3, 3)
            .for_folder("assets")
            .unwrap();
        let glyphs = window.load_font(assets.join("Dyuthi-Regular.ttf")).unwrap();

        let mut game = Game::new(glyphs, dpi_factor, mon_dim);
        while let Some(e) = window.next() {
            game.update_state();
            if let Some(_) = e.render_args() {
                game.render(&mut window, &e);
                break;
            }
        }
        game
    }

    #[test]
    fn test_mouse_click() {
        music::start::<Music, Sound, _>(16, || {
            let mut test_game = game_setup();
            test_game.music_init();
            test_game.current_turn = Turn::Player;
            test_game.board[8].symbol = Turn::Player;
            let x = test_game.board[8].rect[0];
            let y = test_game.board[8].rect[1];
            println!("mouse_click at: ({},{})", x, y);
            assert!(!test_game.mouse_click(x, y));
            assert_eq!(test_game.board[8].symbol, Turn::Player);

            let mut test_game = game_setup();
            test_game.current_turn = Turn::Player;
            test_game.board[1].symbol = Turn::Bot;
            let x = test_game.board[1].rect[0];
            let y = test_game.board[1].rect[1];
            println!("mouse_click at: ({},{})", x, y);
            assert!(!test_game.mouse_click(x, y));
            assert_eq!(test_game.board[1].symbol, Turn::Bot);

            let mut test_game = game_setup();
            test_game.current_turn = Turn::Player;
            test_game.board[4].symbol = Turn::Nobody;
            let x = test_game.board[4].rect[0];
            let y = test_game.board[4].rect[1];
            println!("mouse_click at: ({},{})", x, y);
            assert!(test_game.mouse_click(x, y));
            assert_eq!(test_game.board[4].symbol, Turn::Player);
        });
    }

    #[test]
    fn test_check_for_victory_none() {
        let mut test_game = game_setup();
        test_game.check_for_victory();
        assert_eq!(test_game.winner, Turn::Nobody);
    }

    #[test]
    fn test_check_for_victory_row() {
        let mut test_game = game_setup();
        test_game.board[0].symbol = Turn::Player;
        test_game.board[1].symbol = Turn::Player;
        test_game.board[2].symbol = Turn::Player;
        test_game.check_for_victory();
        assert_eq!(test_game.winner, Turn::Player);

        let mut test_game = game_setup();
        test_game.board[3].symbol = Turn::Player;
        test_game.board[4].symbol = Turn::Player;
        test_game.board[5].symbol = Turn::Player;
        test_game.check_for_victory();
        assert_eq!(test_game.winner, Turn::Player);

        let mut test_game = game_setup();
        test_game.board[6].symbol = Turn::Player;
        test_game.board[7].symbol = Turn::Player;
        test_game.board[8].symbol = Turn::Player;
        test_game.check_for_victory();
        assert_eq!(test_game.winner, Turn::Player);
    }

    #[test]
    fn test_check_for_victory_column() {
        let mut test_game = game_setup();
        test_game.board[0].symbol = Turn::Player;
        test_game.board[3].symbol = Turn::Player;
        test_game.board[6].symbol = Turn::Player;
        test_game.check_for_victory();
        assert_eq!(test_game.winner, Turn::Player);

        let mut test_game = game_setup();
        test_game.board[1].symbol = Turn::Player;
        test_game.board[4].symbol = Turn::Player;
        test_game.board[7].symbol = Turn::Player;
        test_game.check_for_victory();
        assert_eq!(test_game.winner, Turn::Player);

        let mut test_game = game_setup();
        test_game.board[2].symbol = Turn::Player;
        test_game.board[5].symbol = Turn::Player;
        test_game.board[8].symbol = Turn::Player;
        test_game.check_for_victory();
        assert_eq!(test_game.winner, Turn::Player);
    }

    #[test]
    fn test_check_for_victory_diagonal() {
        let mut test_game = game_setup();
        test_game.board[0].symbol = Turn::Player;
        test_game.board[4].symbol = Turn::Player;
        test_game.board[8].symbol = Turn::Player;
        test_game.check_for_victory();
        assert_eq!(test_game.winner, Turn::Player);

        let mut test_game = game_setup();
        test_game.board[2].symbol = Turn::Player;
        test_game.board[4].symbol = Turn::Player;
        test_game.board[6].symbol = Turn::Player;
        test_game.check_for_victory();
        assert_eq!(test_game.winner, Turn::Player);
    }

    #[test]
    fn test_reset_board() {
        let mut test_game = game_setup();
        for i in 0..test_game.board.len() {
            test_game.board[i].symbol = Turn::Player;
        }
        test_game.winner = Turn::Player;
        test_game.reset_board();
        for i in 0..test_game.board.len() {
            assert_eq!(test_game.board[i].symbol, Turn::Nobody);
        }
        assert_eq!(test_game.winner, Turn::Nobody);
    }
}
