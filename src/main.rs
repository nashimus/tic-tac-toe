extern crate music;
extern crate piston_window;

mod game;

use game::Game;
use piston_window::*;

fn main() {
    let mut window: PistonWindow = WindowSettings::new("Tic Tac Toe", [90.0, 90.0])
        .exit_on_esc(true)
        .build()
        .unwrap();

    let mon_result = window.window.ctx.window().current_monitor();

    let mut monitor_dimensions = Size {
        width: 0.0,
        height: 0.0,
    };

    let mut dpi_factor = 1.0;

    match mon_result {
        Some(x) => {
            println!("Current monitor width: {}", x.size().width);
            println!("Current monitor height: {}", x.size().height);
            println!("Current monitor scale_factor: {}", x.scale_factor());
            monitor_dimensions.width = x.size().width.into();
            monitor_dimensions.height = x.size().height.into();
            dpi_factor = x.scale_factor();
        }
        None => {
            println!("Unable to get current monitor.");
        }
    }

    window.set_max_fps(60);

    let new_size = Size {
        width: 0.5 * monitor_dimensions.width / dpi_factor,
        height: 0.5 * monitor_dimensions.width / dpi_factor,
    };
    window.set_size(new_size);

    let x = ((monitor_dimensions.width / dpi_factor) - window.size().width) / 2.0;
    let y = ((monitor_dimensions.height / dpi_factor) - window.size().height) / 2.0 * 0.95;
    window.set_position([x as i32, y as i32]);

    let assets = find_folder::Search::ParentsThenKids(3, 3)
        .for_folder("assets")
        .unwrap();
    let glyphs = window.load_font(assets.join("Dyuthi-Regular.ttf")).unwrap();

    let mut game = Game::new(glyphs, dpi_factor, monitor_dimensions);
    game.run(&mut window);
}
